package xyz.znix.rtlang;

import xyz.znix.rtlang.rt.Comparator;
import xyz.znix.rtlang.rt.TypeException;
import xyz.znix.rtlang.rt.obj.Constant;

/**
 * Created by znix on 6/24/17.
 */
public interface RTLValue {
	RTLValue TRUE = new Constant("true") {
		@Override
		public boolean toBoolean() {
			return true;
		}
	};
	RTLValue FALSE = new Constant("false") {
		@Override
		public boolean toBoolean() {
			return false;
		}
	};
	RTLValue NULL = null;

	default boolean equals(RTLRuntime runtime, RTLValue valB) {
		return equals(valB);
	}

	default boolean toBoolean() {
		throw new TypeException(this, "boolean");
	}

	default RTLValue getField(String fieldName) {
		// TODO FIXME TODO FIXME awful, awful solution. Be more like Ruby
		switch (fieldName) {
			case "overload_?=":
				return Comparator.equals(this);
			case "overload_?!":
				return Comparator.notEquals(this);
			default:
				throw new RuntimeException("TODO fixme");
		}
	}
}
