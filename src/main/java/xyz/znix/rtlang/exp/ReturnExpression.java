package xyz.znix.rtlang.exp;

import xyz.znix.rtlang.RTLRuntime;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.Scope;

/**
 * Created by znix on 27/06/17.
 */
public class ReturnExpression implements Expression {
	private final int skipCount;
	private final Expression retValue;

	public ReturnExpression(int skipCount, Expression retValue) {
		this.skipCount = skipCount;
		this.retValue = retValue;
	}

	@Override
	public ExpressionResult evaluate(RTLRuntime runtime, Scope scope, int steps, RTLValue lastResult, Object userdata) {
		if (steps == 0) {
			return new ExpressionResult.Delegate(retValue);
		}

		return new ExpressionResult.Return(lastResult, skipCount);
	}
}
