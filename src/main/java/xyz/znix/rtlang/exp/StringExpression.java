package xyz.znix.rtlang.exp;

import xyz.znix.rtlang.RTLRuntime;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.Scope;
import xyz.znix.rtlang.rt.obj.StringVal;

/**
 * Created by znix on 6/24/17.
 */
public class StringExpression implements Expression {
	private final String sval;

	public StringExpression(String sval) {
		this.sval = sval;
	}

	@Override
	public ExpressionResult evaluate(RTLRuntime runtime, Scope scope, int steps, RTLValue lastResult, Object userdata) {
		if (steps == 0) {
			return new ExpressionResult.Timer();
		}
		return new ExpressionResult.Complete(new StringVal(sval));
	}
}
