package xyz.znix.rtlang.exp;

import xyz.znix.rtlang.RTLRuntime;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.Scope;

/**
 * A simple infinite loop.
 * <p>
 * Created by znix on 27/06/17.
 */
public class Loop implements Expression {
	private final Expression initializer;
	private final Expression conditional;
	private final Expression function;
	private final Expression postFunc;

	public Loop(Expression initializer, Expression conditional, Expression function, Expression postFunc) {
		this.initializer = initializer;
		this.conditional = conditional;
		this.function = function;
		this.postFunc = postFunc;
	}

	@Override
	public ExpressionResult evaluate(RTLRuntime runtime, Scope scope, int steps, RTLValue lastResult, Object userdata) {
		// First, run the initializer
		if (steps == 0 && initializer != null)
			return new ExpressionResult.Delegate(initializer);

		Data data = (Data) userdata;
		if (data == null) data = new Data(null, State.INIT);

		if (data.state == State.FUNC) {
			data.mainFuncValue = lastResult;
		}

		switch (data.state) {
			case FUNC:
				if (postFunc != null) {
					data.state = State.POST;
					return new ExpressionResult.Delegate(postFunc, data);
				}
			case INIT:
			case POST:
				data.state = State.COND;
				return new ExpressionResult.Delegate(conditional, data);
			case COND:
				if (!lastResult.toBoolean()) {
					return new ExpressionResult.Complete(data.mainFuncValue);
				}
				data.state = State.FUNC;
				return new ExpressionResult.Delegate(function, data);
			default:
				throw new IllegalStateException();
		}
	}

	@Override
	public boolean localScope() {
		return true;
	}

	@Override
	public boolean catchesReturns() {
		return true;
	}

	private class Data {
		RTLValue mainFuncValue;
		State state;

		public Data(RTLValue mainFuncValue, State state) {
			this.mainFuncValue = mainFuncValue;
			this.state = state;
		}
	}

	private enum State {
		INIT, COND, FUNC, POST
	}
}
