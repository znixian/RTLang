package xyz.znix.rtlang.exp;

import xyz.znix.rtlang.RTLRuntime;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.Scope;

/**
 * Created by znix on 6/24/17.
 */
public class ReadVariable implements Expression {
	private String varName;

	public ReadVariable(String varName) {
		this.varName = varName;
	}

	@Override
	public ExpressionResult evaluate(RTLRuntime runtime, Scope scope, int steps, RTLValue lastResult, Object userdata) {
		if (steps == 0) {
			return new ExpressionResult.Timer();
		}

		return new ExpressionResult.Complete(scope.get(varName));
	}
}
