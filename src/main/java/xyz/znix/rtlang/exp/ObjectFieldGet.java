package xyz.znix.rtlang.exp;

import xyz.znix.rtlang.RTLRuntime;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.Scope;

/**
 * Created by znix on 6/25/17.
 */
public class ObjectFieldGet implements Expression {
	private final Expression object;
	private final String fieldName;

	public ObjectFieldGet(Expression object, String fieldName) {
		this.object = object;
		this.fieldName = fieldName;
	}

	@Override
	public ExpressionResult evaluate(RTLRuntime runtime, Scope scope, int steps, RTLValue lastResult, Object userdata) {
		if (steps == 0) {
			return new ExpressionResult.Timer();
		}

		if (steps == 1) {
			return new ExpressionResult.Delegate(object);
		}

		// TODO timing for the getField event
		return new ExpressionResult.Complete(lastResult.getField(fieldName));
	}
}
