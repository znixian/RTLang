package xyz.znix.rtlang.exp;

import xyz.znix.rtlang.RTLRuntime;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.Scope;

/**
 * Should use ?= or ?!
 * <p>
 * Created by znix on 6/24/17.
 */
@Deprecated
public class EqualsComparison implements Expression {
	private final Expression paramA;
	private final Expression paramB;

	public EqualsComparison(Expression paramA, Expression paramB) {
		this.paramA = paramA;
		this.paramB = paramB;
	}

	@Override
	public ExpressionResult evaluate(RTLRuntime runtime, Scope scope, int steps, RTLValue lastResult, Object userdata) {
		if (steps == 0) {
			return new ExpressionResult.Delegate(paramA);
		}

		if (steps == 1) {
			return new ExpressionResult.Delegate(paramB, lastResult);
		}

		RTLValue valA = (RTLValue) userdata;

		// TODO some kind of timing for equals
		RTLValue result = valA.equals(runtime, lastResult) ? RTLValue.TRUE : RTLValue.FALSE;
		return new ExpressionResult.Complete(result);
	}
}
