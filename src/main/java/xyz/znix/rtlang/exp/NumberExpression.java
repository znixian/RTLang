package xyz.znix.rtlang.exp;

import xyz.znix.rtlang.RTLRuntime;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.Scope;
import xyz.znix.rtlang.rt.obj.NumberVal;

/**
 * Created by znix on 6/24/17.
 */
public class NumberExpression implements Expression {
	private final double num;

	public NumberExpression(double num) {
		this.num = num;
	}

	@Override
	public ExpressionResult evaluate(RTLRuntime runtime, Scope scope, int steps, RTLValue lastResult, Object userdata) {
		if (steps == 0) {
			return new ExpressionResult.Timer();
		}

		return new ExpressionResult.Complete(new NumberVal(num));
	}

	public double getNum() {
		return num;
	}
}
