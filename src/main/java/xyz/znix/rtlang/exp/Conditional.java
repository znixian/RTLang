package xyz.znix.rtlang.exp;

import xyz.znix.rtlang.RTLProgram;
import xyz.znix.rtlang.RTLRuntime;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.Scope;

/**
 * Created by znix on 6/24/17.
 */
public class Conditional implements Expression {
	private final Expression condition;
	private final RTLProgram ifTrue;
	private final RTLProgram ifFalse;

	public Conditional(Expression condition, RTLProgram ifTrue, RTLProgram ifFalse) {
		this.condition = condition;
		this.ifTrue = ifTrue;
		this.ifFalse = ifFalse;
	}

	@Override
	public ExpressionResult evaluate(RTLRuntime runtime, Scope scope, int steps, RTLValue lastResult, Object userdata) {
		if (steps == 0) {
			return new ExpressionResult.Timer();
		}

		if (steps == 1) {
			return new ExpressionResult.Delegate(condition);
		}

		RTLProgram target = lastResult.toBoolean() ? ifTrue : ifFalse;
		if (target == null) {
			return new ExpressionResult.Complete(RTLValue.NULL);
		}
		return new ExpressionResult.Switch(target);
	}
}
