package xyz.znix.rtlang.exp;

import xyz.znix.rtlang.RTLRuntime;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.Scope;
import xyz.znix.rtlang.rt.TypeException;
import xyz.znix.rtlang.rt.obj.Function;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by znix on 6/24/17.
 */
public class FunctionCallExpression implements Expression {
	private final Expression func;
	private final List<Expression> args;

	public FunctionCallExpression(Expression func, List<Expression> args) {
		this.func = func;
		this.args = args;
	}

	@Override
	public ExpressionResult evaluate(RTLRuntime runtime, Scope scope, int steps, RTLValue lastResult, Object userdata) {
		int size = args.size();
		List<RTLValue> evalArgs = (List<RTLValue>) userdata;

		// From the second step on, we are getting the results of evaluating the arguments
		if (steps >= 2 && steps < size + 2) {
			evalArgs.add(lastResult);
		}

		// On the first step, we just wait for our delay
		if (steps == 0) {
			return new ExpressionResult.Timer(new ArrayList<RTLValue>());
		} else if (size + 1 == steps) {
			return new ExpressionResult.Delegate(this.func, userdata);
		} else if (size + 2 == steps) {
			if (lastResult instanceof Function) {
				Expression exp = ((Function) lastResult).call(runtime, scope, evalArgs);
				assert exp != null;
				return new ExpressionResult.Switch(exp);
			} else {
				throw new TypeException(lastResult, "function");
			}
		}

		// Then, for each of the arguments, we evaluate them:
		int argIndex = steps - 1;
		return new ExpressionResult.Delegate(args.get(argIndex), userdata);
	}
}
