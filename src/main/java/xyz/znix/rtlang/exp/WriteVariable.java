package xyz.znix.rtlang.exp;

import xyz.znix.rtlang.RTLRuntime;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.Scope;

/**
 * Created by znix on 6/24/17.
 */
public class WriteVariable implements Expression {
	private final String name;
	private final Expression expression;

	public WriteVariable(String name, Expression expression) {
		this.name = name;
		this.expression = expression;
	}

	@Override
	public ExpressionResult evaluate(RTLRuntime runtime, Scope scope, int steps, RTLValue lastResult, Object userdata) {
		if (steps == 0) {
			return new ExpressionResult.Timer();
		}

		if(steps == 1) {
			return new ExpressionResult.Delegate(expression);
		}

		scope.put(name, lastResult);
		return new ExpressionResult.Complete(lastResult);
	}
}
