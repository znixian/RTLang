package xyz.znix.rtlang.exp;

import xyz.znix.rtlang.RTLProgram;
import xyz.znix.rtlang.RTLRuntime;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.Scope;
import xyz.znix.rtlang.rt.obj.ProgramFunction;

import java.util.List;

/**
 * Created by znix on 6/24/17.
 */
public class ExpressedFunction implements Expression {
	private final String name;
	private final List<String> args;
	private final RTLProgram program;

	public ExpressedFunction(String name, List<String> args, RTLProgram program) {
		this.name = name;
		this.args = args;
		this.program = program;
	}

	@Override
	public ExpressionResult evaluate(RTLRuntime runtime, Scope scope, int steps, RTLValue lastResult, Object userdata) {
		if (steps == 0) {
			return new ExpressionResult.Timer();
		}

		ProgramFunction func = new ProgramFunction(program, args);

		if (name != null) {
			runtime.getGlobals().put(name, func);
		}

		return new ExpressionResult.Complete(func);
	}
}
