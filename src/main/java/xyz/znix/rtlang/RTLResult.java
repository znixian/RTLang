package xyz.znix.rtlang;

/**
 * Created by znix on 6/25/17.
 */
public interface RTLResult {
	RTLValue getReturnValue();
	long getTotalTicks();
}
