package xyz.znix.rtlang;

import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.Scope;

import java.util.*;

/**
 * Created by znix on 6/24/17.
 */
public class RTLProgram implements AutoCloseable, Expression {
	private List<Expression> instructions = new ArrayList<>();
	private boolean scoped;
	private boolean closed;

	public RTLProgram(Expression... all) {
		List<Expression> toAdd = Arrays.asList(all);
		instructions.addAll(toAdd);
	}

	/**
	 * Closes and returns this object, to be used when chaining.
	 *
	 * @return {@code this}
	 */
	public RTLProgram closeChain() {
		close();
		return this;
	}

	/**
	 * Close this object, preventing it from being modified in the future.
	 */
	public void close() {
		if (isClosed()) throw new IllegalStateException("Cannot call close() twice!");
		instructions = Collections.unmodifiableList(instructions);
		closed = true;
	}

	public boolean add(Expression expression) {
		return instructions.add(expression);
	}

	public boolean addAll(Collection<? extends Expression> c) {
		return instructions.addAll(c);
	}

	public List<Expression> getInstructions() {
		return instructions;
	}

	public boolean isClosed() {
		return closed;
	}

	public void setScoped(boolean scoped) {
		checkOpen();
		this.scoped = scoped;
	}

	private void checkOpen() {
		if (closed) {
			throw new IllegalStateException("Cannot modify closed program!");
		}
	}

	private void checkClosed() {
		if (!closed) {
			throw new IllegalStateException("Cannot run open program!");
		}
	}

	@Override
	public ExpressionResult evaluate(RTLRuntime runtime, Scope scope, int steps,
									 RTLValue lastResult, Object userdata) {
		checkClosed();

		if (steps >= getInstructions().size()) {
			return new ExpressionResult.Complete(lastResult);
		}

		return new ExpressionResult.Delegate(getInstructions().get(steps), true);
	}

	@Override
	public boolean localScope() {
		checkClosed();
		return scoped;
	}

	@Override
	public boolean catchesReturns() {
		return true;
	}
}
