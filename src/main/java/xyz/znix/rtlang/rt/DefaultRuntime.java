package xyz.znix.rtlang.rt;

import xyz.znix.rtlang.RTLContext;
import xyz.znix.rtlang.RTLResult;
import xyz.znix.rtlang.RTLRuntime;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.obj.Function;

import java.util.*;

import static xyz.znix.rtlang.compiler.Expression.ExpressionResult.*;
import static xyz.znix.rtlang.compiler.Expression.ExpressionResult.Timer;

/**
 * Created by znix on 6/24/17.
 */
public class DefaultRuntime implements RTLRuntime {
	private final Scope globals = new GlobalScope();

	public DefaultRuntime() {
		// TODO
		globals.put("print", (Function) (runtime, scope, evalArgs) -> {
			String output = toRtlString(evalArgs);
			System.out.println(output);
			return null;
		});

		// TODO prevent these from being changed
		globals.put("true", RTLValue.TRUE);
		globals.put("false", RTLValue.FALSE);
	}

	public Map<String, RTLValue> getGlobals() {
		return globals;
	}

	public Context eval(Expression expression, Scope scope) {
		Context ctx = new Context();
		ctx.addExpression(expression, scope);

		return ctx;
	}

	@Override
	public RTLResult step(RTLContext context, int numSteps) {
		while (numSteps != 0) {
			Context ctx = (Context) context;
			int stackSize = ctx.stack.size();
			Context.StackItem si = ctx.stack.get(stackSize - 1);

			if (si.timer > 0) {
				if (numSteps == -1) {
					si.timer = 0;
				} else {
					int rem = Math.min(si.timer, numSteps);
					ctx.globalTimer += rem;
					si.timer -= rem;
					numSteps -= rem;
					continue;
				}
			}

			Expression.ExpressionResult result =
					si.expr.evaluate(this, si.scope, si.step, si.lastResult, si.userdata);

			si.step++;
			si.userdata = result.getUserdata();

			if (result instanceof Complete) {
				RTLValue value = ((Complete) result).getValue();

				if (stackSize == 1) {
					return new RTLResult() {
						@Override
						public RTLValue getReturnValue() {
							return value;
						}

						@Override
						public long getTotalTicks() {
							return ctx.globalTimer;
						}
					};
				}

				ctx.stack.remove(stackSize - 1);
				Context.StackItem prev = ctx.stack.get(stackSize - 2);

				prev.lastResult = value;
			} else if (result instanceof Timer) {
				si.timer = ((Timer) result).getTimer();
				if (si.timer == -1)
					si.timer = getDelay(si.expr);
			} else if (result instanceof Delegate) {
				boolean isSwitch = result instanceof Switch;
				Delegate dg = (Delegate) result;
				assert dg.getNext() != null;
				Scope scope = si.scope;
				if (dg.getNext().localScope()) {
					scope = new LocalScope(scope);
				}
				if (isSwitch)
					ctx.stack.remove(stackSize - 1);
				ctx.addExpression(dg.getNext(), scope);
			} else if (result instanceof Return) {
				Return ret = (Return) result;
				RTLValue val = ret.getReturnValue();
				int stackTop = stackSize - 1;

				// Remove the expression that just called return
				ctx.stack.remove(stackTop--);

				for (int i = 0; i < ret.getLevels(); ) {
					if (stackTop < 0) {
						throw new IllegalArgumentException("Cannot return "
								+ ret.getLevels() + " levels - stack underflow!");
					}

					Context.StackItem frame = ctx.stack.remove(stackTop);
					stackTop--;

					if (frame.expr.catchesReturns()) {
						i++;
					}
				}

				// If the program was returned from
				if (stackTop == -1) {
					return new RTLResult() {
						@Override
						public RTLValue getReturnValue() {
							return val;
						}

						@Override
						public long getTotalTicks() {
							return ctx.globalTimer;
						}
					};
				}

				ctx.stack.get(stackTop).lastResult = val;
			} else throw new UnsupportedOperationException(
					"Unrecognised option " + result.getClass() + "," + result);
		}
		return null;
	}

	@Override
	public int getDelay(Expression type) {
		// TODO use decent values
		return 1;
	}

	@Override
	public Scope getDefaultScope() {
		return globals;
	}

	private static class Context implements RTLContext {
		private long globalTimer;
		private List<StackItem> stack = new ArrayList<>();

		private static class StackItem {
			private Scope scope;
			private Expression expr;

			private RTLValue lastResult;
			private int timer;
			private int step;
			private Object userdata;

			public String toString() {
				return expr.toString();
			}
		}

		private void addExpression(Expression expr, Scope scope) {
			StackItem si = new StackItem();
			stack.add(si);

			si.expr = expr;
			si.scope = scope;
		}
	}

	public static String toRtlString(List<RTLValue> evalArgs) {
		return evalArgs.stream()
				.map(Objects::toString)
				.reduce((a, b) -> a + ", " + b).orElse("");
	}

	private class GlobalScope extends HashMap<String, RTLValue> implements Scope {
		@Override
		public void putLocal(String key, RTLValue value) {
			put(key, value);
		}
	}

	private class LocalScope implements Scope {
		private final Scope parent;
		private final Map<String, RTLValue> localVariables = new HashMap<>();

		private LocalScope(Scope parent) {
			this.parent = parent;
		}

		@Override
		public int size() {
			return keySet().size();
		}

		@Override
		public boolean isEmpty() {
			return parent.isEmpty() && localVariables.isEmpty();
		}

		@Override
		public boolean containsKey(Object key) {
			return localVariables.containsKey(key) || parent.containsKey(key);
		}

		@Override
		public boolean containsValue(Object value) {
			return localVariables.containsValue(value) || parent.containsValue(value);
		}

		@Override
		public RTLValue get(Object key) {
			if (!(key instanceof String)) return null;

			if (localVariables.containsKey(key)) {
				return localVariables.get(key);
			}

			return parent.get(key);
		}

		@Override
		public RTLValue put(String key, RTLValue value) {
			if (!localVariables.containsKey(key) && parent.containsKey(key))
				return parent.put(key, value);
			else
				return localVariables.put(key, value);
		}

		@Override
		public void putLocal(String key, RTLValue value) {
			localVariables.put(key, value);
		}

		@Override
		public RTLValue remove(Object key) {
			return localVariables.remove(key);
		}

		@Override
		public void putAll(Map<? extends String, ? extends RTLValue> m) {
			m.forEach(this::put);
		}

		@Override
		public void clear() {
			localVariables.clear();
		}

		@Override
		public Set<String> keySet() {
			HashSet<String> keys = new HashSet<>(parent.keySet());
			keys.addAll(localVariables.keySet());

			return keys;
		}

		@Override
		public Collection<RTLValue> values() {
			throw new UnsupportedOperationException();
		}

		@Override
		public Set<Entry<String, RTLValue>> entrySet() {
			Set<Entry<String, RTLValue>> es = new HashSet<>(localVariables.entrySet());

			parent.entrySet().stream()
					.filter(e -> !localVariables.containsKey(e.getKey()))
					.forEach(es::add);

			return es;
		}
	}
}
