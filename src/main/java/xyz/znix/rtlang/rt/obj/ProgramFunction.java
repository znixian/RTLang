package xyz.znix.rtlang.rt.obj;

import xyz.znix.rtlang.RTLProgram;
import xyz.znix.rtlang.RTLRuntime;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.Scope;

import java.util.List;

/**
 * Created by znix on 6/24/17.
 */
public class ProgramFunction implements Function {
	private final RTLProgram program;
	private final List<String> args;

	public ProgramFunction(RTLProgram program, List<String> args) {
		this.program = program;
		this.args = args;
	}

	@Override
	public Expression call(RTLRuntime runtime, Scope scope, List<RTLValue> evalArgs) {
		return new FunctionProgramWrapper(program, args, evalArgs);
	}

	private static class FunctionProgramWrapper extends RTLProgram {
		private final List<String> args;
		private final List<RTLValue> argsValues;

		public FunctionProgramWrapper(RTLProgram program, List<String> args, List<RTLValue> argsValues) {
			this.args = args;
			this.argsValues = argsValues;

			getInstructions().addAll(program.getInstructions());
			setScoped(true);
			close();
		}

		@Override
		public ExpressionResult evaluate(RTLRuntime runtime, Scope scope, int steps,
										 RTLValue lastResult, Object userdata) {
			if (steps == 0 && args != null && argsValues != null) {
				for (int i = 0; i < args.size(); i++) {
					scope.putLocal(args.get(i),
							i >= argsValues.size() ? null : argsValues.get(i));
				}
			}

			return super.evaluate(runtime, scope, steps, lastResult, userdata);
		}
	}
}
