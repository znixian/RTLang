package xyz.znix.rtlang.rt.obj;

import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.exp.NumberExpression;

/**
 * Created by znix on 6/24/17.
 */
public class NumberVal implements RTLValue {
	private final double num;

	public NumberVal(double num) {
		this.num = num;
	}

	public String toString() {
		return "" + num;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		NumberVal numberVal = (NumberVal) o;

		return Double.compare(numberVal.num, num) == 0;
	}

	@Override
	public int hashCode() {
		long temp = Double.doubleToLongBits(num);
		return (int) (temp ^ (temp >>> 32));
	}

	@Override
	public RTLValue getField(String fieldName) {
		if ("overload_+".equals(fieldName)) {
			return (Function) (runtime, scope, evalArgs) -> {
				// TODO
				if (evalArgs.size() != 1) throw new RuntimeException("Bad length");
				NumberVal val = (NumberVal) evalArgs.get(0);

				return new NumberExpression(num + val.num);
			};
		}
		return RTLValue.super.getField(fieldName);
	}
}
