package xyz.znix.rtlang.rt.obj;

import xyz.znix.rtlang.RTLValue;

/**
 * Created by znix on 6/24/17.
 */
public class Constant implements RTLValue {
	private String name;

	public Constant(String name) {
		this.name = name;
	}

	public String toString() {
		return name;
	}
}
