package xyz.znix.rtlang.rt.obj;

import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.exp.StringExpression;

/**
 * Created by znix on 6/24/17.
 */
public class StringVal implements RTLValue {
	private String value;

	public StringVal(String value) {
		this.value = value;
	}

	public String toString() {
		return value;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		StringVal stringVal = (StringVal) o;

		return value != null ? value.equals(stringVal.value) : stringVal.value == null;
	}

	@Override
	public int hashCode() {
		return value != null ? value.hashCode() : 0;
	}

	@Override
	public RTLValue getField(String fieldName) {
		if ("overload_+".equals(fieldName)) {
			return (Function) (runtime, scope, evalArgs) -> {
				// TODO
				if(evalArgs.size() != 1) throw new RuntimeException("Bad length");

				return new StringExpression(value + evalArgs.get(0).toString());
			};
		}
		return RTLValue.super.getField(fieldName);
	}
}
