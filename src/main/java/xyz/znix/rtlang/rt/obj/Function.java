package xyz.znix.rtlang.rt.obj;

import xyz.znix.rtlang.RTLRuntime;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.Scope;

import java.util.List;

/**
 * Created by znix on 6/24/17.
 */
public interface Function extends RTLValue {
	Expression call(RTLRuntime runtime, Scope scope, List<RTLValue> evalArgs);
}
