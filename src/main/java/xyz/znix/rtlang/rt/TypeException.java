package xyz.znix.rtlang.rt;

import xyz.znix.rtlang.RTLValue;

/**
 * Created by znix on 6/24/17.
 */
public class TypeException extends RTLRuntimeException {
	public TypeException(RTLValue from, String targetType) {
		super("Cannot convert from " + from.toString() + "/"
				+ from.getClass().getSimpleName() + " to " + targetType);
	}
}
