package xyz.znix.rtlang.rt;

import xyz.znix.rtlang.RTLRuntime;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.obj.Function;

/**
 * Created by znix on 27/06/17.
 */
public class Comparator {
	private Comparator() {
	}

	public static Function equals(RTLValue base) {
		return (runtime, scope, evalArgs) ->
				new Exp(rt -> base.equals(rt, evalArgs.get(0)) ? RTLValue.TRUE : RTLValue.FALSE);
	}

	public static Function notEquals(RTLValue base) {
		return (runtime, scope, evalArgs) ->
				new Exp(rt -> base.equals(rt, evalArgs.get(0)) ? RTLValue.FALSE : RTLValue.TRUE);
	}

	private static class Exp implements Expression {

		private ValProvider func;

		public Exp(ValProvider func) {
			this.func = func;
		}

		@Override
		public ExpressionResult evaluate(RTLRuntime runtime, Scope scope, int steps,
										 RTLValue lastResult, Object userdata) {
			return new ExpressionResult.Complete(func.provide(runtime));
		}
	}

	private interface ValProvider {
		RTLValue provide(RTLRuntime rt);
	}
}
