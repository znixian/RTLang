package xyz.znix.rtlang.rt;

import xyz.znix.rtlang.RTLValue;

import java.util.Map;

/**
 * Created by znix on 6/25/17.
 */
public interface Scope extends Map<String, RTLValue> {
	void putLocal(String key, RTLValue value);
}
