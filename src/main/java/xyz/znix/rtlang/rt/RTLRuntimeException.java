package xyz.znix.rtlang.rt;

/**
 * Created by znix on 6/24/17.
 */
public class RTLRuntimeException extends RuntimeException {
	public RTLRuntimeException(String message) {
		super(message);
	}
}
