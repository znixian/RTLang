package xyz.znix.rtlang;

import xyz.znix.rtlang.compiler.Expression;
import xyz.znix.rtlang.rt.Scope;

import java.util.Map;

/**
 * Created by znix on 6/24/17.
 */
public interface RTLRuntime {
	Map<String, RTLValue> getGlobals();

	default RTLContext run(RTLProgram program, Scope scope) {
		return eval(program, scope);
	}

	default RTLContext run(RTLProgram program) {
		return run(program, getDefaultScope());
	}

	RTLContext eval(Expression expression, Scope scope);

	/**
	 * Step the supplied program by a set number of ticks,
	 * or until completion if {@code numSteps} is set to {@code -1}.
	 *
	 * @param context The program to run.
	 * @param numSteps The number of steps to step the program by.
	 * @return If the program is complete, the results of the execution. Otherwise, {@code null}
	 */
	RTLResult step(RTLContext context, int numSteps);

	int getDelay(Expression type);

	Scope getDefaultScope();
}
