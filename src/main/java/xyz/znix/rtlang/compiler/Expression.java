package xyz.znix.rtlang.compiler;

import xyz.znix.rtlang.RTLRuntime;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.rt.Scope;

/**
 * Created by znix on 6/24/17.
 */
public interface Expression {
	Expression NULL = (runtime, scope, steps, lastResult, userdata) -> new ExpressionResult.Complete(RTLValue.NULL);

	ExpressionResult evaluate(RTLRuntime runtime, Scope scope, int steps, RTLValue lastResult, Object userdata);

	/**
	 * Should this expression be run in a new scope?
	 */
	default boolean localScope() {
		return false;
	}

	/**
	 * Should return statements be able to land on this expression?
	 *
	 * @return {@code true} if return statements can land on this expression,
	 * {@code false} if return statements should skip over.
	 */
	default boolean catchesReturns() {
		return false;
	}

	class ExpressionResult {
		private final Object userdata;

		private ExpressionResult() {
			this(null);
		}

		private ExpressionResult(Object userdata) {
			this.userdata = userdata;
		}

		public static class Complete extends ExpressionResult {
			private final RTLValue value;

			public Complete(RTLValue value) {
				this.value = value;
			}

			public Complete(RTLValue value, Object userdata) {
				super(userdata);
				this.value = value;
			}

			public RTLValue getValue() {
				return value;
			}
		}

		public static class Timer extends ExpressionResult {
			private final int timer;

			public Timer() {
				this(-1);
			}

			public Timer(int timer) {
				this.timer = timer;
			}

			public Timer(Object userdata) {
				this(-1, userdata);
			}

			public Timer(int timer, Object userdata) {
				super(userdata);
				this.timer = timer;
			}

			public int getTimer() {
				return timer;
			}
		}

		public static class Delegate extends ExpressionResult {
			private final Expression next;

			public Delegate(Expression next) {
				this.next = next;
			}

			public Delegate(Expression next, Object userdata) {
				super(userdata);
				this.next = next;
			}

			public Expression getNext() {
				return next;
			}
		}

		public static class Switch extends Delegate {
			public Switch(Expression next) {
				super(next);
			}
		}

		public static class Return extends ExpressionResult {
			private final RTLValue returnValue;
			private final int levels;

			public Return(RTLValue returnValue, int levels) {
				this.returnValue = returnValue;
				this.levels = levels;
			}

			public RTLValue getReturnValue() {
				return returnValue;
			}

			public int getLevels() {
				return levels;
			}
		}

		public Object getUserdata() {
			return userdata;
		}

	}

}
