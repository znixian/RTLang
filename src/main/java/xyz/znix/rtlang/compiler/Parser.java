package xyz.znix.rtlang.compiler;

import java.io.IOException;
import java.io.StreamTokenizer;

import static java.io.StreamTokenizer.*;

/**
 * Created by znix on 6/24/17.
 */
public class Parser {
	private final StreamTokenizer input;

	public Parser(StreamTokenizer input) {
		this.input = input;
	}

	// Expecters
	public String readString() throws ParseException {
		nextToken();
		if (!isString())
			typeError("string");
		return input.sval;
	}

	public double readNumber() throws ParseException {
		assertToken(TT_NUMBER);
		return input.nval;
	}

	public String readWord() throws ParseException {
		assertToken(TT_WORD);
		return input.sval;
	}

	public void assertWord(String target) throws ParseException {
		String s = readWord();
		if (!s.equals(target))
			typeError(target, s);
	}

	public void assertToken(int target) throws ParseException {
		assertToken(target, humanizeTokenType(target));
	}

	public void assertToken(int target, String expected) throws ParseException {
		assertToken(nextToken(), target, expected);
	}

	// Helpers
	public boolean testToken(int target) throws ParseException {
		return peekToken() == target;
	}

	/**
	 * Consume a token if it exists, otherwise do nothing. This is the same as {@link #testToken(int)},
	 * only it does not push back if the token exists.
	 *
	 * @param token The token to find.
	 * @return {@code true} if the token was consumed, {@code false} otherwise.
	 */
	public boolean consumeToken(int token) throws ParseException {
		if (nextToken() == token) {
			return true;
		} else {
			pushBack();
			return false;
		}
	}

	public boolean isString() {
		return input.ttype == '\'' || input.ttype == '"';
	}

	public void pushBack() {
		input.pushBack();
	}

	public boolean isEOF() throws ParseException {
		boolean status = nextToken() == TT_EOF;
		pushBack();
		return status;
	}

	public int nextToken() throws ParseException {
		try {
			int tok = input.nextToken();
			return tok;
		} catch (IOException e) {
			throw new ParseInputException(e);
		}
	}

	public int peekToken() throws ParseException {
		nextToken();
		pushBack();
		return input.ttype;
	}

	// Private helpers
	private void assertToken(int actual, int target, String expected) throws ParseException {
		if (target == actual) return;

		// Okay, we have a problem.
		typeError(expected);
	}

	private void typeError(String expected) throws SyntaxException {
		typeError(expected, humanizeTokenType(input.ttype));
	}

	public static void typeError(String expected, String actual) throws SyntaxException {
		throw new SyntaxException("Error parsing: expected " + expected + ", got " + actual);
	}

	public static String humanizeTokenType(int value) {
		switch (value) {
			case TT_WORD:
				return "WORD";
			case TT_EOF:
				return "End of file";
			case TT_NUMBER:
				return "Number";
			default:
				return "'" + ((char) value) + "'";
		}
	}

	public StreamTokenizer getInput() {
		return input;
	}
}
