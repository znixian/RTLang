package xyz.znix.rtlang.compiler;

import java.io.IOException;
import java.io.Reader;

/**
 * Created by znix on 6/24/17.
 */
public class ColumnTrackerReader extends Reader {
	private int numRead;
	private Reader input;

	public ColumnTrackerReader(Reader input) {
		this.input = input;
	}

	@Override
	public boolean ready() throws IOException {
		return input.ready();
	}

	@Override
	public int read(char[] cbuf, int off, int len) throws IOException {
		numRead += len;
		return input.read(cbuf, off, len);
	}

	@Override
	public void close() throws IOException {
		input.close();
	}

	public int getNumRead() {
		return numRead;
	}
}
