package xyz.znix.rtlang.compiler;

/**
 * Created by znix on 6/24/17.
 */
public interface StatementEndChecker {
	boolean check() throws ParseException;
}
