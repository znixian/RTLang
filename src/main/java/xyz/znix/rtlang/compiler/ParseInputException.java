package xyz.znix.rtlang.compiler;

import java.io.IOException;

/**
 * Created by znix on 6/24/17.
 */
public class ParseInputException extends ParseException {
	public ParseInputException(IOException e) {
		super(e);
	}
}
