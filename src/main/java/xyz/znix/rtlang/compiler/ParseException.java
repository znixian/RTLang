package xyz.znix.rtlang.compiler;

/**
 * Created by znix on 6/24/17.
 */
public abstract class ParseException extends Exception {
	public ParseException(String message, Throwable cause) {
		super(message, cause);
	}

	public ParseException(Throwable cause) {
		super(cause);
	}

	public ParseException(String s) {
		super(s);
	}
}
