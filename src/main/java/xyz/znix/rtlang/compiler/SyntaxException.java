package xyz.znix.rtlang.compiler;

/**
 * Created by znix on 6/24/17.
 */
public class SyntaxException extends ParseException {
	public SyntaxException(String s) {
		super(s);
	}
}
