package xyz.znix.rtlang;

import xyz.znix.rtlang.compiler.*;
import xyz.znix.rtlang.exp.*;

import java.io.Reader;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.io.StreamTokenizer.TT_NUMBER;
import static java.io.StreamTokenizer.TT_WORD;

/**
 * Created by znix on 6/24/17.
 */
public class RTLCompiler {
	public RTLCompiler() {

	}

	public RTLProgram compile(String input) throws ParseException {
		return compile(new StringReader(input));
	}

	public RTLProgram compile(Reader input) throws ParseException {
		ColumnTrackerReader layer = new ColumnTrackerReader(input);

		// Set up a tokenizer
		StreamTokenizer tk = new StreamTokenizer(layer);

		tk.eolIsSignificant(false);
		tk.slashSlashComments(true);
		tk.slashSlashComments(true);
		tk.ordinaryChar('.'); // 0.25 should still work, but .25 won't.

		Parser parser = new Parser(tk);

		// Set up our program
		RTLProgram program = new RTLProgram();

		try {
			while (!parser.isEOF()) {
				program.add(parseStatement(parser));

				//if (type == StreamTokenizer.TT_EOF) {
				//	// TODO
				//	break;
				//}
			}
		} catch (ParseException ex) {
			System.out.println("Chars read: " + layer.getNumRead());
			throw ex;
		}

		return program.closeChain();
	}

	private Expression parseStatement(Parser parser) throws ParseException {
		Expression exp = parseExpression(parser, () -> parser.testToken(';'));
		parser.assertToken(';');
		return exp;
	}

	private Expression parseExpression(Parser parser, StatementEndChecker check) throws ParseException {
		int token = parser.peekToken();
		Expression expression;

		if (token == TT_WORD) {
			String word = parser.readWord();
			parser.pushBack();
			switch (word) {
				case "function":
					expression = parseFunction(parser);
					break;
				case "if":
				case "unless":
					expression = parseIf(parser);
					break;
				case "return":
					return parseReturn(parser, check);
				case "while":
				case "for":
					expression = parseLoop(parser);
					break;
				default: {
					parser.nextToken();
					if (parser.consumeToken('=')) {
						return new WriteVariable(word, parseExpression(parser, check));
					} else {
						expression = new ReadVariable(word);
					}
				}
			}
		} else if (token == TT_NUMBER) {
			double num = parser.readNumber();
			expression = new NumberExpression(num);
		} else if (parser.isString()) {
			expression = new StringExpression(parser.readString());
		} else {
			throw new SyntaxException("Unknown token '" + ((char) token) + "'");
		}

		while (!check.check())
			expression = parseExpressionPart(expression, parser, check);

		return expression;
	}

	private Expression parseExpressionPart(Expression base, Parser parser, StatementEndChecker check) throws ParseException {
		int token = parser.peekToken();
		if (token == TT_WORD) {
			String statement = parser.readWord();
			parser.pushBack();

			switch (statement) {
				default:
					throw new SyntaxException("word: " + statement); // TODO
			}
		} else if (token == '(' || token == '!') {
			return parseMethodCall(base, parser, check);
		} else if (token == '=') {
			parser.nextToken();
			int next = parser.nextToken();

			if (next == '=') {
				System.out.println("WARNING: == for comparison is deprecated. Use ?= instead.");
				return new EqualsComparison(base, parseExpression(parser, check));
			} else {
				throw new SyntaxException("Bad comparison =" + (char) next); // TODO
			}
		} else if (token == '.') {
			parser.nextToken();
			int ref = parser.peekToken();
			String fieldName;
			if (ref == TT_WORD) {
				fieldName = parser.readWord();
			} else if (ref > 0) {
				fieldName = "overload_" + (char) parser.nextToken();
			} else {
				throw new SyntaxException("char: " + token); // TODO
			}

			return new ObjectFieldGet(base, fieldName);
		} else if (token == '+' || token == '-' || token == '?') {
			String ch = "" + (char) token;
			parser.nextToken();
			if (token == '?') {
				ch += (char) parser.nextToken();
			}
			return parseMethodCallInterior(
					new ObjectFieldGet(base, "overload_" + ch),
					parser,
					check,
					() -> false
			);
		} else {
			throw new SyntaxException("char: " + (char) token + " (" + token + ")"); // TODO
		}
	}

	private Expression parseFunction(Parser parser) throws ParseException {
		// Read the 'function' keyword
		parser.assertWord("function");

		// See if this function declaration has a name
		String name;
		if (parser.testToken(TT_WORD)) {
			name = parser.readWord();
		} else {
			name = null;
		}

		// See if there are any arguments
		List<String> args = null;
		if (parser.testToken('(')) {
			parser.nextToken();

			if (!parser.testToken(')')) {
				args = new ArrayList<>();
				do {
					args.add(parser.readWord());
				} while (parser.testToken(','));
			}
			parser.assertToken(')');
		}

		// Read all the expressions
		RTLProgram program = readCodeBlock(parser);

		// Return the result
		return new ExpressedFunction(name, args, program);
	}

	private Expression parseMethodCall(Expression base, Parser parser,
									   StatementEndChecker check) throws ParseException {
		// Ideally, in a as-un-greedy-as-possible way
		boolean usesParentheses = parser.consumeToken('(');
		if (!usesParentheses) parser.assertToken('!', "'!' or '('");

		StatementEndChecker paramChecker;

		if (usesParentheses) {
			paramChecker = () -> parser.testToken(',') || parser.testToken(')');
		} else {
			paramChecker = () -> parser.testToken(',') || check.check();
		}

		Expression exp;
		if (usesParentheses && parser.testToken(')')) {
			exp = new FunctionCallExpression(base, Collections.emptyList());
		} else {
			exp = parseMethodCallInterior(base, parser, paramChecker, () -> parser.consumeToken(','));
		}

		if (usesParentheses)
			parser.assertToken(')');

		return exp;
	}

	private Expression parseMethodCallInterior(Expression base, Parser parser,
											   StatementEndChecker paramChecker,
											   StatementEndChecker nextParamChecker)
			throws ParseException {
		List<Expression> args = new ArrayList<>();

		do {
			args.add(parseExpression(
					parser,
					paramChecker
			));
		} while (nextParamChecker.check());

		return new FunctionCallExpression(base, args);
	}

	private Expression parseLoop(Parser parser) throws ParseException {
		String keyword = parser.readWord();

		if ("while".equals(keyword)) {
			// Read in the conditional block
			parser.assertToken('(');
			Expression condition = parseExpression(parser, () -> parser.testToken(')'));
			parser.assertToken(')');

			// Read the block
			RTLProgram program = readCodeBlock(parser);

			return new Loop(null, condition, program, null);
		} else if ("for".equals(keyword)) {
			// Read in the arguments block
			parser.assertToken('(');
			Expression initializer = parseExpression(parser, () -> parser.testToken(';'));
			parser.assertToken(';');
			Expression conditional = parseExpression(parser, () -> parser.testToken(';'));
			parser.assertToken(';');
			Expression postLoop = parseExpression(parser, () -> parser.testToken(')'));
			parser.assertToken(')');

			// Read the main block
			RTLProgram program = readCodeBlock(parser);

			// Return the loop
			return new Loop(initializer, conditional, program, postLoop);
		} else {
			throw new RuntimeException("Not a if or unless keyword!");
		}
	}

	private Expression parseIf(Parser parser) throws ParseException {
		String keyword = parser.readWord();
		boolean inverted = false;

		if ("if".equals(keyword)) {
			inverted = false;
		} else if ("unless".equals(keyword)) {
			inverted = true;
		} else {
			throw new RuntimeException("Not a if or unless keyword!");
		}

		// Read in the conditional block
		parser.assertToken('(');
		Expression condition = parseExpression(parser, () -> parser.testToken(')'));
		parser.assertToken(')');

		// Read the main block
		RTLProgram program = readCodeBlock(parser);

		// See if there is a else block
		RTLProgram alternative = null;
		if (parser.peekToken() == TT_WORD) {
			if (!parser.readWord().equals("else")) {
				parser.pushBack();
			} else {
				alternative = readCodeBlock(parser);
			}
		}

		// If it was a unless block, flip the conditions around.
		if (inverted) {
			RTLProgram tmp = program;
			program = alternative;
			alternative = tmp;
		}

		return new Conditional(condition, program, alternative);
	}

	private Expression parseReturn(Parser parser, StatementEndChecker check) throws ParseException {
		parser.assertWord("return");

		int skipCount = 1;

		// Read the first value
		Expression retValue = parseExpression(parser, () -> parser.testToken(',') || check.check());

		if (parser.consumeToken(',')) {
			if (!(retValue instanceof NumberExpression)) {
				throw new SyntaxException("If a level count is supplied to return, it must be a fixed interger >=1");
			}

			// Read in the conditional block
			double dSkipCount = ((NumberExpression) retValue).getNum();
			skipCount = (int) dSkipCount;

			if (dSkipCount != skipCount || skipCount < 1) {
				// TODO use different exception?
				throw new SyntaxException("Skip count must be int >= 1");
			}

			// Read the new expression
			retValue = parseExpression(parser, check);
		}

		return new ReturnExpression(skipCount, retValue);
	}

	private RTLProgram readCodeBlock(Parser parser) throws ParseException {
		boolean block = parser.testToken('{');

		if (block) {
			// Check for the opening bracket
			parser.assertToken('{');

			// TODO deduplicate this
			RTLProgram program = new RTLProgram();
			while (!parser.testToken('}')) {
				Expression expression = parseStatement(parser);
				program.add(expression);
			}
			program.close();

			// Read the closing bracket
			parser.assertToken('}');

			return program;
		} else {
			Expression expression = parseStatement(parser);
			return new RTLProgram(expression).closeChain();
		}
	}
}
