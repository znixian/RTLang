package test;

import org.junit.Test;
import xyz.znix.rtlang.RTLProgram;
import xyz.znix.rtlang.compiler.ParseException;
import xyz.znix.rtlang.rt.obj.StringVal;

import static org.junit.Assert.assertEquals;
import static test.AppTest.*;

/**
 * Created by znix on 6/24/17.
 */
public class IfTest {
	@Test
	public void testIf() throws ParseException {
		RTLProgram program = compile("if(1==1) 'foo'; else 'bar';;");
		assertEquals("if should work", new StringVal("foo"), run(program).val);
	}

	@Test
	public void testUnless() throws ParseException {
		RTLProgram program = compile("unless(1==1) 'bar'; else 'foo';;");
		assertEquals("if should work", new StringVal("foo"), run(program).val);
	}

	@Test
	public void testEmbeddedIf() throws ParseException {
		RTLProgram program = compile("if(1?=1) function ()'hi';; else function {'blah';};();");
		assertEquals("if should work", new StringVal("hi"), run(program).val);
	}

	@Test
	public void testNotEquals() throws ParseException {
		RTLProgram program = compile("unless(1?!1) 'foo'; else 'bar';;");
		assertEquals("comparator ?! should work", new StringVal("foo"), run(program).val);
	}
}
