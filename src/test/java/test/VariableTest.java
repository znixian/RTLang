package test;

import org.junit.Test;
import xyz.znix.rtlang.RTLProgram;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.ParseException;
import xyz.znix.rtlang.rt.obj.NumberVal;
import xyz.znix.rtlang.rt.obj.StringVal;

import static org.junit.Assert.assertEquals;
import static test.AppTest.*;

/**
 * Created by znix on 6/25/17.
 */
public class VariableTest {
	@Test
	public void testScopeIsolation() throws ParseException {
		RTLProgram program = compile("function() a=1;(); a;");
		assertEquals("variables should be isolated from their parent scopes", RTLValue.NULL, run(program).val);
	}

	@Test
	public void testOverwriteParent() throws ParseException {
		RTLProgram program = compile("a='bad'; function() a=1;(); a;");
		assertEquals("setting variables in a scope should overwrite variables in a parent scope",
				new NumberVal(1), run(program).val);
	}

	@Test
	public void testParamsIsolated() throws ParseException {
		RTLProgram program = compile("a='good'; function(a) a='evil';(123); a;");
		assertEquals("function parameters should not interfere with parent scope variables",
				new StringVal("good"), run(program).val);
	}

	@Test
	public void testRunAtCallTime() throws ParseException {
		RTLProgram program = compile("a=0;function func() a=a+1;;func();func();a;");
		assertEquals("run multiple times", new NumberVal(2), run(program).val);
	}

	@Test
	public void testCorrectParameters() throws ParseException {
		RTLProgram program = compile("a=0;function(b)a=b;(1234);a;");
		assertEquals("values passed as function arguments must appear correctly",
				new NumberVal(1234), run(program).val);
	}

}
