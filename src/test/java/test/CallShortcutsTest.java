package test;

import org.junit.Test;
import xyz.znix.rtlang.RTLProgram;
import xyz.znix.rtlang.compiler.ParseException;
import xyz.znix.rtlang.rt.obj.NumberVal;
import xyz.znix.rtlang.rt.obj.StringVal;

import static org.junit.Assert.assertEquals;
import static test.AppTest.compile;
import static test.AppTest.run;

/**
 * Created by znix on 26/06/17.
 */
public class CallShortcutsTest {
	@Test
	public void testAdd() throws ParseException {
		RTLProgram program = compile("1 + 2;");
		assertEquals("adding numbers", new NumberVal(3), run(program).val);
	}

	@Test
	public void testAddString() throws ParseException {
		RTLProgram program = compile("'1' + '2' + '3';");
		assertEquals("adding strings", new StringVal("123"), run(program).val);
	}
}
