package test;

import org.junit.Test;
import xyz.znix.rtlang.RTLProgram;
import xyz.znix.rtlang.RTLValue;
import xyz.znix.rtlang.compiler.ParseException;
import xyz.znix.rtlang.rt.obj.NumberVal;
import xyz.znix.rtlang.rt.obj.StringVal;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static test.AppTest.compile;
import static test.AppTest.run;

/**
 * Created by znix on 27/06/17.
 */
public class LoopTest {
	@Test
	public void testInfWhileLoop() throws ParseException {
		RTLProgram program = compile("a=0;b='';while(true){a=a+1;b=b+'a';if(a?=10) return 3,b;;};");
		assertEquals("infinite loop should have made 10 calls",
				new StringVal("aaaaaaaaaa"), run(program).val);
	}

	@Test
	public void testWhileLoopReturn() throws ParseException {
		RTLProgram program = compile("while(true) {return 2,1;}+1;");
		assertEquals("return(2) should return value from while loop", new NumberVal(2), run(program).val);
	}

	@Test
	public void testWhileLoopCondition() throws ParseException {
		RTLProgram program = compile("a=0;while(a ?! 5) {a=a+1; 100 + a;};");
		assertEquals("while loops", new NumberVal(105), run(program).val);
	}

	@Test
	public void testForLoop() throws ParseException {
		RTLProgram program = compile("a=0;for(i=0;i?!5;i=i+1) a=a+1;; a;");
		assertEquals("for loop iterate correct number of times", new NumberVal(5), run(program).val);
	}

	@Test
	public void testForLoopScope() throws ParseException {
		RTLProgram program = compile("for(i=0;i?!5;i=i+1){}; i;");
		assertEquals("for loop initializer should be run in scope", RTLValue.NULL, run(program).val);
	}

	@Test
	public void testForLoopContents() throws ParseException {
		RTLProgram program = compile("for(i=0;i?!5;i=i+1){print(i);i;};");
		assertEquals("for loop initializer should be run in scope", new AppTest.RunResult(
				new NumberVal(4),
				Arrays.asList(
						"0.0",
						"1.0",
						"2.0",
						"3.0",
						"4.0"
				)
		), run(program));
	}
}
