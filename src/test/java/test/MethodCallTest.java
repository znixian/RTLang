package test;

import org.junit.Test;
import xyz.znix.rtlang.RTLProgram;
import xyz.znix.rtlang.compiler.ParseException;
import xyz.znix.rtlang.rt.obj.NumberVal;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static test.AppTest.*;

/**
 * Created by znix on 6/25/17.
 */
public class MethodCallTest {
	@Test
	public void testParentheses() throws ParseException {
		RTLProgram program = compile("1.0.+(2);");
		assertEquals("with parentheses 1+2=3", new NumberVal(3), run(program).val);
	}

	@Test
	public void testNoParentheses() throws ParseException {
		RTLProgram program = compile("1 .+! 2;");
		assertEquals("without parenthesis 1+2=3", new NumberVal(3), run(program).val);
	}

	@Test
	public void testCommaOrder() throws ParseException {
		RTLProgram program = compile("print! 1, 2, print! 3, 4, print! 5, 6;");
		assertEquals("without parenthesis 1+2=3", Arrays.asList(
				"5.0, 6.0",
				"3.0, 4.0, null",
				"1.0, 2.0, null"
		), run(program).print);
	}
}
